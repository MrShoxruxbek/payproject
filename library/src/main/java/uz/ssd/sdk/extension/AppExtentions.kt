package uz.ssd.sdk.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import toothpick.Toothpick
import uz.ssd.sdk.di.DI
import uz.ssd.sdk.di.factory.ViewModelFactory
import toothpick.Scope

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>): T =
    ViewModelProviders.of(this, Toothpick.openScope(DI.APP_SCOPE)
        .getInstance(ViewModelFactory::class.java))
        .get(viewModelClass)

fun <T : ViewModel> Fragment.obtainViewModel(viewModelClass: Class<T>, scope: Scope) =
    ViewModelProviders.of(this, Toothpick.openScope(DI.APP_SCOPE)
        .getInstance(ViewModelFactory::class.java))
        .get(viewModelClass)