package uz.ssd.sdk.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import timber.log.Timber
import uz.ssd.sdk.entity.server.BaseRequest
import java.security.MessageDigest
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

internal fun Context.calculateNoOfColumns(columnWidthDp: Float): Int {
    val displayMetrics = resources.displayMetrics
    val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
    return (screenWidthDp / columnWidthDp + 0.5).toInt()
}

internal fun Context.color(colorRes: Int) = ContextCompat.getColor(this, colorRes)

internal fun Context.drawable(drawableRes: Int): Drawable {
    return ContextCompat.getDrawable(this, drawableRes)!!.mutate()
}

internal fun Context.getTintDrawable(drawableRes: Int, colorRes: Int): Drawable {
    val source = ContextCompat.getDrawable(this, drawableRes)!!.mutate()
    val wrapped = DrawableCompat.wrap(source)
    DrawableCompat.setTint(wrapped, color(colorRes))
    return wrapped
}

internal fun EditText.setTint(colorRes: Int) {
    val constantState = background.constantState ?: return
    val drwNewCopy = constantState.newDrawable().mutate()
    val wrapped = DrawableCompat.wrap(drwNewCopy)
    DrawableCompat.setTint(wrapped, context.color(colorRes))
    ViewCompat.setBackground(this, wrapped)
}

internal fun Context.getTintDrawable(drawableRes: Int, colorResources: IntArray, states: Array<IntArray>): Drawable {
    val source = ContextCompat.getDrawable(this, drawableRes)!!.mutate()
    val wrapped = DrawableCompat.wrap(source)
    DrawableCompat.setTintList(wrapped, ColorStateList(states, colorResources.map { color(it) }.toIntArray()))
    return wrapped
}

internal fun TextView.setStartDrawable(drawable: Drawable) {
    setCompoundDrawablesWithIntrinsicBounds(
        drawable,
        null,
        null,
        null
    )
}

internal fun TextView.setEndDrawable(drawable: Drawable) {
    setCompoundDrawablesWithIntrinsicBounds(
        null,
        null,
        drawable,
        null
    )
}

internal fun TextView.setStartAndEndDrawable(startDrawable: Drawable, endDrawable: Drawable) {
    setCompoundDrawablesWithIntrinsicBounds(
        startDrawable,
        null,
        endDrawable,
        null
    )
}

internal fun ImageView.tint(colorRes: Int) = this.setColorFilter(this.context.color(colorRes))

internal fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

internal fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

internal fun TextView.showTextOrHide(str: String?) {
    this.text = str
    this.visible(!str.isNullOrBlank())
}

internal fun Fragment.tryOpenLink(link: String?, basePath: String? = "https://google.com/search?q=") {
    if (link != null) {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    when {
                        URLUtil.isValidUrl(link) -> Uri.parse(link)
                        else -> Uri.parse(basePath + link)
                    }
                )
            )
        } catch (e: Exception) {
            Timber.e("tryOpenLink error: $e")
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://google.com/search?q=$link")
                )
            )
        }
    }
}

fun Fragment.showSnackMessage(message: String) {
    view?.showSnackMessage(message)
}

fun View.showSnackMessage(message: String) {
    val ssb = SpannableStringBuilder().apply {
        append(message)
        setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            message.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    Snackbar.make(this, ssb, Snackbar.LENGTH_LONG).show()
}

/**
 * Return whether screen is landscape.
 *
 * @return {@code true}: yes<br>{@code false}: no
 */
internal fun Fragment.isLandscape() = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

/**
 * Return whether device is tablet.
 *
 * @return {@code true}: yes<br>{@code false}: no
 */
internal fun Fragment.isTablet() =
    (resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE

internal fun Activity.hideKeyboard() {
    currentFocus?.apply {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

internal fun Any.objectScopeName() = "${javaClass.simpleName}_${hashCode()}"

internal fun Boolean.toInt() = if (this) 1 else 0

internal fun Int.toTiyin(): Long = this * 100L

internal fun Long.toTiyin(): Long = this * 100L

internal fun Long.tiyinToSum(): Long = this / 100L

internal fun Long.formattedMoney(showDecimal: Boolean = true, tiyinToSum: Boolean = true) =
    formatMoney(if (tiyinToSum) (this / 100.toDouble()) else this.toDouble(), showDecimal)

internal fun Double.formattedMoney(showDecimal: Boolean = true) = formatMoney(this, showDecimal)

internal fun String.formattedMoney(showDecimal: Boolean = true) = formatMoney(this.toDouble(), showDecimal)

internal fun Float.formattedMoney(showDecimal: Boolean = true) = formatMoney(this.toDouble(), showDecimal)

internal fun Int.formattedMoney(showDecimal: Boolean = true) = formatMoney(this.toDouble(), showDecimal)

internal fun String.formattedCardNumber(): String {
    if (length != 16) {
        return this
    }
    val stringBuilder = StringBuilder(this)
    stringBuilder.insert(4, " ")
    stringBuilder.insert(9, " ")
    stringBuilder.insert(14, " ")
    return stringBuilder.toString()
}

internal fun String.formattedCardNumberNew(): String {
    val stringBuilder = StringBuilder(this)
    if (length == 13) {
        if (substring(6, 9) == "***") {
            stringBuilder.replace(6, 9, "******")
        }
    }
    if (stringBuilder.length != 16) {
        return this
    }
    stringBuilder.insert(4, " ")
    stringBuilder.insert(9, " ")
    stringBuilder.insert(14, " ")
    return stringBuilder.toString()
}

internal fun String.formatToMaskedCardPan(): String {
    if (length != 16) {
        return this
    }
    val stringBuilder = StringBuilder(this)
    stringBuilder.replace(6, 12, "••••••")
    stringBuilder.insert(4, " ")
    stringBuilder.insert(9, " ")
    stringBuilder.insert(14, " ")
    return stringBuilder.toString()
}

internal fun String.formatBKMPinCard(): String {
    val stringBuilder = StringBuilder(this)
    val size = length / 3
    for (i in 1..size) {
        if (i * 3 == length) break
        stringBuilder.insert(i * 3 + i - 1, " ")
    }
    return stringBuilder.toString()
}

internal fun formatMoney(value: Double, showDecimal: Boolean): String {
    val decimalFormat = DecimalFormat("###,###,##0.00")
    decimalFormat.groupingSize = 3
    decimalFormat.minimumFractionDigits = 0

    val s = DecimalFormatSymbols()
    s.groupingSeparator = ' '
    val symbols = decimalFormat.decimalFormatSymbols
    s.decimalSeparator = symbols.decimalSeparator
    decimalFormat.decimalFormatSymbols = s

    decimalFormat.minimumFractionDigits = if (showDecimal) 2 else 0
    decimalFormat.maximumFractionDigits = if (showDecimal) 2 else 0

    return decimalFormat.format(value)
}

internal fun <T> Moshi.toJson(type: Class<T>, value: T): String = adapter(type).toJson(value)

internal inline fun <reified DATA> BaseRequest<DATA>.getSdkHash(moshi: Moshi): String {
    val type = Types.newParameterizedType(BaseRequest::class.java, DATA::class.java)
    val adapter: JsonAdapter<BaseRequest<DATA>> = moshi.adapter(type)
    return "Walla"
}

internal inline fun <reified DATA> BaseRequest<DATA>.getSdkHash(moshi: Moshi, auth: String): String {
    val type = Types.newParameterizedType(BaseRequest::class.java, DATA::class.java)
    val adapter: JsonAdapter<BaseRequest<DATA>> = moshi.adapter(type)
    val json = adapter.toJson(this)
    return "Walla"
}

internal fun makeAuthHeader(): String {
    val timestamp = System.currentTimeMillis().toString()
    return timestamp
}

internal fun sha1(input: String): String {
    return hashString("SHA-1", input)
}

/**
 * Supported algorithms on Android:
 *
 * Algorithm	Supported API Levels
 * MD5          1+
 * SHA-1	    1+
 * SHA-224	    1-8,22+
 * SHA-256	    1+
 * SHA-384	    1+
 * SHA-512	    1+
 */
private fun hashString(type: String, input: String): String {
    val HEX_CHARS = "0123456789abcdef"
    val bytes = MessageDigest
        .getInstance(type)
        .digest(input.toByteArray())
    val result = StringBuilder(bytes.size * 2)

    bytes.forEach {
        val i = it.toInt()
        result.append(HEX_CHARS[i shr 4 and 0x0f])
        result.append(HEX_CHARS[i and 0x0f])
    }

    return result.toString()
}