package uz.ssd.sdk

import android.content.Context
import android.content.Intent
import androidx.annotation.Keep
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import uz.ssd.sdk.di.DI
import uz.ssd.sdk.di.module.AppModule
import uz.ssd.sdk.di.module.ServerModule
import uz.ssd.sdk.model.system.LocaleManager
import uz.ssd.sdk.ui.AppActivity

object SSDBilling {

    /**
     * @param context: Application context
     */
    @[JvmStatic Keep]
    fun init(context: Context) {
        LocaleManager.saveLanguage(context)
        initLogger()
        initToothpick()
        initAppScope(context)
    }


    @[JvmStatic Keep]
    fun createPaymentForServicesIntent(context: Context): Intent {
        return Intent(context, AppActivity::class.java).putExtra("screen", 1)
    }

    @JvmStatic
    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    @JvmStatic
    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    @JvmStatic
    private fun initAppScope(context: Context) {
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(context))
        appScope.installModules(ServerModule())
    }

}