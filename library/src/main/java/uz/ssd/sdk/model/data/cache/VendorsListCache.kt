package uz.ssd.sdk.model.data.cache

import timber.log.Timber
import uz.ssd.sdk.di.CacheLifetime
import uz.ssd.sdk.di.PrimitiveWrapper
import uz.ssd.sdk.entity.form.Result
import javax.inject.Inject

internal class VendorsListCache @Inject constructor(
    @CacheLifetime lifetimeWrapper: PrimitiveWrapper<Long>
) {
    private val lifetime = lifetimeWrapper.value

    private var cache: Result? = null
    private var time: Long = -1

    fun clear() {
        Timber.d("Clear cache")
        cache = null
    }

    fun put(data: Result) {
        Timber.d("Put vendors list")
        time = System.currentTimeMillis()
        cache = data
    }

    fun get(): Result? {
        return if (cache == null || System.currentTimeMillis() - time > lifetime) {
            Timber.d("Get NULL vendors list")
            cache = null
            null
        } else {
            Timber.d("Get CACHED vendors list")
            cache
        }

    }

}