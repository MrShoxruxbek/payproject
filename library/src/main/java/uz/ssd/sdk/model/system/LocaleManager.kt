package uz.ssd.sdk.model.system

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import java.util.*

object LocaleManager {

    private const val LANGUAGE_KEY = "language_key"

    fun setLocale(c: Context?): Context? {
        if (c == null) return null
        return updateResources(c, getLanguage(c)) // Locale.getDefault()
    }

    @JvmStatic
    fun setLocaleRu(c: Context?): Context? {
        if (c == null) return null
        val savedLanguage = "ru"
        return updateResources(c, savedLanguage)
    }

    fun saveLanguage(c: Context) {
        val currentLocale = getLocale(c.resources)
        saveLanguage(c, currentLocale.toString())
    }

    @SuppressLint("ApplySharedPref")
    fun saveLanguage(c: Context, language: String) {
        val prefs = c.getSharedPreferences("LocaleConf", Context.MODE_PRIVATE)
        prefs.edit().putString(LANGUAGE_KEY, language).commit()
    }

    fun getLanguage(c: Context): String {
        val prefs = c.getSharedPreferences("LocaleConf", Context.MODE_PRIVATE)
        val currentLocale = getLocale(c.resources)
        return prefs.getString(LANGUAGE_KEY, currentLocale.toString())!!
    }

    fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        return updateResources(context, locale)
    }

    private fun updateResources(context: Context, locale: Locale): Context {
        var mContext = context
        val res = mContext.resources
        val config = Configuration(res.configuration)
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale)
            mContext = mContext.createConfigurationContext(config)
            // I need to set this so that recall setText(R.string.xxxx) works
//            res.updateConfiguration(config, res.displayMetrics)
        } else {
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)
        }
        return mContext
    }

    private fun getLocale(res: Resources): Locale {
        val config = res.configuration
        return if (Build.VERSION.SDK_INT >= 24) config.locales.get(0) else config.locale
    }

}
