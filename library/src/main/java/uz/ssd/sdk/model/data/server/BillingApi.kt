package uz.ssd.sdk.model.data.server

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import uz.ssd.sdk.entity.form.GenerateCheque
import uz.ssd.sdk.entity.form.Result
import uz.ssd.sdk.entity.server.BaseMethod
import uz.ssd.sdk.entity.server.BaseRequest
import uz.ssd.sdk.entity.server.BaseResponse
import uz.ssd.sdk.entity.vendor.VendorFormAccount

internal interface BillingApi {

    companion object {
        const val API_PATH = "/api"
    }

    /**
     * Categories with vendors
     */
    @POST(API_PATH)
    fun getAllCategory(
        @Body req: BaseMethod
    ): Single<BaseResponse<Result>>

    /**
     * Vendor form check
     */
    @POST(API_PATH)
    fun vendorFormCheck(
        @Body req: BaseRequest<VendorFormAccount>
    ): Single<BaseResponse<GenerateCheque>>

}