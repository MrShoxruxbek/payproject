package uz.ssd.sdk.model.system

import android.content.Context
import javax.inject.Inject

internal class ResourceManager @Inject constructor(private val context: Context) {

    fun getString(id: Int): String = context.getString(id)

    fun getString(id: Int, vararg formatArgs: Any) = String.format(context.getString(id, *formatArgs))
}