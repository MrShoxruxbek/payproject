package uz.ssd.sdk.model.interactor

import io.reactivex.Single
import uz.ssd.sdk.entity.server.BaseMethod
import uz.ssd.sdk.entity.server.BaseRequest
import uz.ssd.sdk.entity.vendor.CategoryVendor
import uz.ssd.sdk.entity.vendor.Vendor
import uz.ssd.sdk.entity.vendor.VendorForm
import uz.ssd.sdk.entity.vendor.VendorFormAccount
import uz.ssd.sdk.model.data.server.BillingApi
import uz.ssd.sdk.model.data.server.ServerError
import uz.ssd.sdk.model.system.SchedulersProvider
import javax.inject.Inject

internal class VendorInteractor @Inject constructor(
    private val api: BillingApi,
    private val schedulers: SchedulersProvider
) {

    fun getAllCategory(): Single<List<CategoryVendor>> {
        val request = BaseMethod("merchants.get_all")
        return api
            .getAllCategory(request).map {
                if (it.isFailure()) throw ServerError(it.error!!)
                val result = it.result!!
                val data = ArrayList<CategoryVendor>()
                result.types!!.forEach { type ->
                    data.add(CategoryVendor(
                        id = type.type!!,
                        title = type.title!!,
                        vendors = result.merchants!!.map {
                            Vendor(
                                it.id,
                                it.name!!,
                                it.type.toString(),
                                it.logo!!
                            )
                        }.filter { it.shortName!!.toInt() == type.type }
                    ))
                }
                data.filter { category -> category.vendors.isNotEmpty() }
            }.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }


    fun getVendorForm(
        params: String
    ): Single<List<VendorForm>> {
        val request = BaseMethod("merchants.get_all")
        return api
            .getAllCategory(request).map {
                if (it.isFailure()) throw ServerError(it.error!!)

                val items = it.result?.merchants!!
                val terminalObject = items.find {
                    it.id == params
                }

                val newData = ArrayList<VendorForm>()

                terminalObject?.terminal?.account?.forEach {
                    newData.add(
                        VendorForm(
                            key = it.name,
                            label = it.title,
                            type = it.content,
                            element = it.type,
                            prefix = it.prefix,
                            regex = it.validation,
                            visible = it.visible,
                            size = it.length ?: 0,
                            require = it.require,
                            requires = it.requires,
                            value = it.name,
                            mask = "",

                            placeholder = it.placeholder,
                            // TODO Logic
                            text = if (terminalObject.terminal.type == "link") terminalObject.terminal.endpoint else null,
                            link = if (terminalObject.terminal.type == "link") terminalObject.terminal.endpoint else null,

                            options = it.values,
                            error = it.validationError
                        )
                    )
                }
                if (terminalObject?.terminal?.type == "form") {
                    newData.add(
                        VendorForm(
                            key = "amount",
                            label = "Сумма",
                            element = "text",
                            type = "price",
                            regex = "^[0-9]{1,10}(.)[0-9]{1,2}$",
                            value = "",
                            placeholder = "",
                            amountType = "uzs",
                            minAmount = terminalObject.terminal.amount?.min?.div(100),
                            maxAmount = terminalObject.terminal.amount?.max?.div(100)
                        )
                    )
                }
                if (terminalObject?.terminal?.type == "link") {
                    newData.add(
                        VendorForm(
                            key = terminalObject.name,
                            label = terminalObject.organization,
                            element = "link",
                            type = null,
                            regex = null,
                            value = "",
                            placeholder = "",
                            text = terminalObject.terminal.linkType
                        )
                    )
                }
                newData.toList()
            }.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }


    fun vendorFormCheck(
        params: VendorFormAccount
    ): Single<String> {
        val request = BaseRequest("cheque.create", params)
        return api.vendorFormCheck(request).map {
            if (it.isFailure()) throw ServerError(it.error!!)
            val result = it.result!!
            result.cheque.id
        }.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

}