package uz.ssd.sdk.model.data.server

import io.reactivex.Single
import uz.ssd.sdk.entity.form.Result
import uz.ssd.sdk.entity.server.BaseMethod
import uz.ssd.sdk.entity.server.BaseResponse
import uz.ssd.sdk.model.data.cache.VendorsListCache

internal class ApiWithCache(
    private val serverApi: BillingApi,
    private val vendorsListCache: VendorsListCache
) : BillingApi by serverApi {

    override fun getAllCategory(req: BaseMethod): Single<BaseResponse<Result>> =
        Single
            .defer {
                val cachedCategories = vendorsListCache.get()
                if (cachedCategories == null) {
                    serverApi.getAllCategory(req)
                        .doOnSuccess {
                            val result = it.result
                            if (result != null && !result.merchants.isNullOrEmpty()) {
                                vendorsListCache.put(result)
                            }
                        }
                } else {
                    Single.just(
                        BaseResponse(
                            "2.0",
                            Result(cachedCategories.merchants, cachedCategories.types),
                            null
                        )
                    )
                }
            }
}