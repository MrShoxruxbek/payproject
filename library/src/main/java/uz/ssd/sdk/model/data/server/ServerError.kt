package uz.ssd.sdk.model.data.server

import uz.ssd.sdk.entity.server.ErrorResponse

internal class ServerError(val error: ErrorResponse) : RuntimeException()
