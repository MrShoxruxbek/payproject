package uz.ssd.sdk.model.system.message

internal data class SystemMessage(
    val text: String,
    val type: SystemMessageType = SystemMessageType.ALERT
)