package uz.ssd.sdk.model.system.message

internal enum class SystemMessageType {
    ALERT,
    TOAST
}