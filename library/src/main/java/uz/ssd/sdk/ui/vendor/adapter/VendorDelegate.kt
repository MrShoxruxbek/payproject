package uz.ssd.sdk.ui.vendor.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_vendor.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.Vendor
import uz.ssd.sdk.extension.inflate
import uz.ssd.sdk.ui.global.ButtonClick

internal class VendorDelegate(
    private val clickListener: (Vendor) -> Unit
) : AdapterDelegate<MutableList<Vendor>>() {

    override fun isForViewType(items: MutableList<Vendor>, position: Int) = true

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_vendor))

    override fun onBindViewHolder(
        items: MutableList<Vendor>,
        position: Int,
        viewHolder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) =
        (viewHolder as ViewHolder).bind(items[position])

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is ViewHolder) {
            Glide.with(holder.itemView)
                .clear(holder.ivVendorLogo)
        }
    }

    private inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        private lateinit var vendor: Vendor

        init {
            itemView.setOnClickListener(object : ButtonClick() {
                override fun onSingleClick(v: View?) {
                    clickListener(vendor)
                }
            })
        }

        fun bind(vendor: Vendor) {
            this.vendor = vendor
            Glide.with(itemView)
                .load(vendor.logo)
                .into(ivVendorLogo)
        }
    }
}