package uz.ssd.sdk.ui.vendor.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import uz.ssd.sdk.entity.vendor.Vendor

internal class VendorsListAdapter(
    clickListener: (Vendor) -> Unit,
    vendorsList: List<Vendor>
) : ListDelegationAdapter<MutableList<Vendor>>() {

    init {
        items = vendorsList.toMutableList()
        delegatesManager.addDelegate(VendorDelegate(clickListener))
    }

    fun setData(vendorsList: List<Vendor>) {
        val oldData = items.toList()

        items.clear()
        items.addAll(vendorsList)

        //yes, on main thread...
        DiffUtil
            .calculateDiff(DiffCallback(items, oldData), false)
            .dispatchUpdatesTo(this)
    }

    private inner class DiffCallback(
        private val newItems: List<Vendor>,
        private val oldItems: List<Vendor>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldItems.size
        override fun getNewListSize() = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return newItem.id == oldItem.id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return newItem.logo == oldItem.logo
        }
    }
}