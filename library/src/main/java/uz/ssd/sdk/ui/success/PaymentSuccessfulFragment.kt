package uz.ssd.sdk.ui.success

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_payment_successful.*
import toothpick.Toothpick
import uz.ssd.sdk.R
import uz.ssd.sdk.extension.argument
import uz.ssd.sdk.model.system.message.SystemMessageNotifier
import uz.ssd.sdk.ui.global.BaseFragment
import uz.ssd.sdk.ui.global.ButtonClick
import javax.inject.Inject

internal class PaymentSuccessfulFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_payment_successful

    private val cheque: String by argument(ARG_CHEQUE)

    @Inject
    lateinit var systemMessageNotifier: SystemMessageNotifier

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doneButton.setOnClickListener(object : ButtonClick() {
            override fun onSingleClick(v: View?) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse("http://checkout.paycom.uz/$cheque")
                startActivity(openURL)
            }
        })
        exitButton.setOnClickListener(object : ButtonClick() {
            override fun onSingleClick(v: View?) {
                activity?.finish()
            }
        })
    }

    override fun onBackPressed() {
        Navigation
            .findNavController(requireActivity(), R.id.nav_host_fragment)
            .popBackStack()
    }

    companion object {
        const val ARG_CHEQUE = "merchant_cheque"
    }
}