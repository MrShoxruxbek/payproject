package uz.ssd.sdk.ui.vendor.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import uz.ssd.sdk.entity.vendor.CategoryVendor
import uz.ssd.sdk.entity.vendor.Vendor

internal class CategoryVendorsListAdapter(
    vendorClickListener: (Vendor) -> Unit
) : ListDelegationAdapter<MutableList<Any>>() {

    init {
        items = mutableListOf()
        delegatesManager.addDelegate(CategoryVendorDelegate(vendorClickListener))
    }

    fun setData(categoryVendorsList: List<Any>) {
        val oldData = items.toList()

        items.clear()
        items.addAll(categoryVendorsList)

        //yes, on main thread...
        DiffUtil
            .calculateDiff(DiffCallback(items, oldData), false)
            .dispatchUpdatesTo(this)
    }

    private inner class DiffCallback(
        private val newItems: List<Any>,
        private val oldItems: List<Any>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldItems.size
        override fun getNewListSize() = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return if (oldItem is CategoryVendor && newItem is CategoryVendor) {
                oldItem.id == newItem.id
            } else {
                false
            }
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return if (oldItem is CategoryVendor && newItem is CategoryVendor) {
                oldItem.id == newItem.id && oldItem.vendors.size == newItem.vendors.size
            } else false
        }
    }
}