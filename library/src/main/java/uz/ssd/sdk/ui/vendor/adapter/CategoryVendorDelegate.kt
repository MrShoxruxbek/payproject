package uz.ssd.sdk.ui.vendor.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_category_vendor.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.CategoryVendor
import uz.ssd.sdk.entity.vendor.Vendor
import uz.ssd.sdk.extension.calculateNoOfColumns
import uz.ssd.sdk.extension.inflate
import uz.ssd.sdk.extension.visible
import uz.ssd.sdk.ui.global.decoration.linear.EndOffsetItemDecoration
import uz.ssd.sdk.ui.global.decoration.linear.StartOffsetItemDecoration

internal class CategoryVendorDelegate(
    private val vendorClickListener: (Vendor) -> Unit
) : AdapterDelegate<MutableList<Any>>() {
    private val viewPool = RecyclerView.RecycledViewPool()

    override fun isForViewType(items: MutableList<Any>, position: Int) =
        items[position] is CategoryVendor

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_category_vendor))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        viewHolder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) = (viewHolder as ViewHolder).bind(items[position] as CategoryVendor)

    private inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        private lateinit var categoryVendor: CategoryVendor
        private var showArrowImg: Boolean = false
        private val vendorsSpanCount: Int = itemView.context.calculateNoOfColumns(115F)
        private val startOffsetItemDecoration =
            StartOffsetItemDecoration(itemView.context.resources.getDimensionPixelOffset(R.dimen.vendor_list_start_offset))
        private val endOffsetItemDecoration =
            EndOffsetItemDecoration(itemView.context.resources.getDimensionPixelOffset(R.dimen.vendor_list_end_offset))

        init {
            itemView.setOnClickListener {
                if (!showArrowImg) return@setOnClickListener
                if (rvVendors.layoutManager is GridLayoutManager) {
                    ivArrow.animate().rotation(0f).start()
                    rvVendors.layoutManager = LinearLayoutManager(
                        rvVendors.context,
                        RecyclerView.HORIZONTAL,
                        false
                    )
                    addLinearItemDecoration()
                } else {
                    ivArrow.animate().rotation(180f).start()
                    clearLinearItemDecoration()
                    rvVendors.layoutManager = GridLayoutManager(
                        rvVendors.context,
                        vendorsSpanCount
                    )
                }
            }
        }

        fun bind(categoryVendor: CategoryVendor) {
            this.categoryVendor = categoryVendor
            this.showArrowImg = vendorsSpanCount < categoryVendor.vendors.size
            ivArrow.visible(showArrowImg)
            tvCategoryTitle.text = categoryVendor.title
            ivArrow.rotation = 0f
            rvVendors.apply {
                layoutManager = LinearLayoutManager(
                    context,
                    RecyclerView.HORIZONTAL,
                    false
                )
                adapter = VendorsListAdapter(vendorClickListener, categoryVendor.vendors)
                setRecycledViewPool(viewPool)
            }
            clearLinearItemDecoration()
            addLinearItemDecoration()
        }

        private fun addLinearItemDecoration() {
            rvVendors.addItemDecoration(startOffsetItemDecoration)
            rvVendors.addItemDecoration(endOffsetItemDecoration)
        }

        private fun clearLinearItemDecoration() {
            rvVendors.removeItemDecoration(startOffsetItemDecoration)
            rvVendors.removeItemDecoration(endOffsetItemDecoration)
        }
    }
}