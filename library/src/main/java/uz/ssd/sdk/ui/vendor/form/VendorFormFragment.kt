package uz.ssd.sdk.ui.vendor.form

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_vendor_form.*
import kotlinx.android.synthetic.main.layout_zero.*
import kotlinx.coroutines.flow.collect
import toothpick.Toothpick
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.server.ResultWrapper
import uz.ssd.sdk.entity.vendor.Option
import uz.ssd.sdk.entity.vendor.VendorForm
import uz.ssd.sdk.extension.*
import uz.ssd.sdk.model.system.message.SystemMessageNotifier
import uz.ssd.sdk.ui.global.BaseFragment
import uz.ssd.sdk.ui.global.ZeroViewHolder
import uz.ssd.sdk.ui.global.form.*
import uz.ssd.sdk.ui.success.PaymentSuccessfulFragment
import uz.ssd.sdk.view_model.vendor.form.VendorFormViewModel
import javax.inject.Inject

internal class VendorFormFragment : BaseFragment(), FormChangeListener {
    override val layoutRes = R.layout.fragment_vendor_form

    private val vendorId: String by argument(VENDOR_ID)
    private val vendorName: String by argument(VENDOR_NAME)
    private val vendorLogo: String by argument(VENDOR_LOGO)

    @Inject
    lateinit var systemMessageNotifier: SystemMessageNotifier

    private lateinit var viewModel: VendorFormViewModel
    private val vendorFormParams: HashMap<String, String> = hashMapOf()
    private var zeroViewHolder: ZeroViewHolder? = null
    private var isFadeAnimCanceled: Boolean = false
    private var price: Long? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = obtainViewModel(VendorFormViewModel::class.java, scope)
        viewModel.refreshVendorForm(vendorId)
        ivVendorName.text = vendorName
        Glide.with(ivVendorLogo.context)
            .load(vendorLogo)
            .into(ivVendorLogo)

        btnContinue.setOnClickListener {
            activity?.hideKeyboard()
            viewModel.vendorFormCheck(getVendorFormParams(), price!!, vendorId)
        }

        toolbar.setNavigationOnClickListener {
            activity?.hideKeyboard()
            onBackPressed()
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) { viewModel.refreshVendorForm(vendorId) }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.formUiState.collect {
                when (it) {
                    is ResultWrapper.GenericError -> {
                        showMessage(it.error!!)
                    }
                    is ResultWrapper.Success -> {
                        showVendorForm(true, it.value)
                    }
                    is ResultWrapper.Loading -> {
                        showEmptyView(false)
                        showEmptyProgress(it.showProgress)
                    }
                    else -> {
                        showEmptyView(true)
                    }
                }
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.payStatusUiState.collect {
                when (it) {
                    is ResultWrapper.GenericError -> {
                        showMessage(it.error!!)
                    }
                    is ResultWrapper.Success -> {
                        findNavController().navigate(
                            R.id.sucess,
                            bundleOf(PaymentSuccessfulFragment.ARG_CHEQUE to it.value)
                        )
                    }
                    is ResultWrapper.Loading -> {
                        showBlockingProgress(it.showProgress)
                    }
                    else -> {
                        showEmptyView(true)
                    }
                }
            }
        }
    }

    override fun onFormChanged() {
        var isAllFormsValid = true
        for (i in 2 until vendorFormContainer.childCount) {
            val formItem = vendorFormContainer.getChildAt(i) as FormItem
            if (!formItem.isValid()) {
                isAllFormsValid = false
                break
            }
        }
        btnContinue.isEnabled = isAllFormsValid
    }

    private fun getVendorFormParams(): HashMap<String, String> {
        for (i in 2 until vendorFormContainer.childCount) {
            val formItem = vendorFormContainer.getChildAt(i) as FormItem
            if (formItem.getKey() == "amount") price = formItem.getValue().toLong() * 100
            else vendorFormParams[formItem.getKey()] = formItem.getValue()
        }
        return vendorFormParams
    }

    private fun showEmptyView(show: Boolean) {
        if (show) zeroViewHolder?.showEmptyData()
        else zeroViewHolder?.hide()
    }

    private fun showVendorForm(show: Boolean, vendorFormList: List<VendorForm>?) {
        if (show) {
            isFadeAnimCanceled = true
            vendorFormContent.animate().cancel()
            vendorFormContent.fadeIn()
        } else {
            vendorFormContent.animate().cancel()
            vendorFormContent.fadeOut {
                isFadeAnimCanceled
            }
            isFadeAnimCanceled = false
        }
//        if (vendorFormContainer.childCount != 2) return
        var dynamicSelect: FormSelectField? = null
        var grandParentSelect: FormSelectField? = null
        var parentSelect: FormSelectField? = null
        var childSelect: FormSelectField? = null
        var subChildSelect: FormSelectField? = null
        var formInput: FormInputField? = null
        vendorFormList?.forEach {
            if (it.element == null || it.show != 1) {
                if (it.value != null) {
                    vendorFormParams[it.key] = it.value
                }
            } else {
                when (it.element) {
                    "text" -> {
                        if (formInput == null && it.prefix?.type ?: "" == "require") {
                            formInput = FormInputField(context!!, it, this)
                            vendorFormContainer.addView(formInput)
                            formInput!!.tag = it.key
                        } else {
                            val formField = FormInputField(context!!, it, this)
                            vendorFormContainer.addView(formField)
                        }
                    }
                    "select" -> {
                        val formField = FormSelectField(context!!, it, this)
                        if (it.options?.isNotEmpty() == true) {
                            if (grandParentSelect == null) {
                                grandParentSelect = formField
                            } else if (parentSelect == null) {
                                parentSelect = formField
                            } else if (childSelect == null) {
                                childSelect = formField
                            } else {
                                subChildSelect = formField
                            }
                        }
                        formField.tag = it.key
                        vendorFormContainer.addView(formField)
                    }
                    "dynamic-select" -> {
                        val formField = FormSelectField(context!!, it, this)
                        dynamicSelect = formField
                        formField.tag = it.key
                        vendorFormContainer.addView(formField)
                    }
                    "checkbox" -> {
                        vendorFormContainer.addView(FormCheckboxField(context!!, it))
                    }
                    "html" -> {
                        vendorFormContainer.addView(FormHtmlField(context!!, it))
                    }
                    "link" -> {
                        vendorFormContainer.addView(FormLinkField(context!!, it))
                    }
                }
            }
        }

        grandParentSelect?.setOnSelectedListener(object : FormSelectField.FormSelectFieldListener {
            override fun onOptionSelected(option: Option, require: String?) {
                parentSelect?.notifyDataSetChange()
                if (dynamicSelect?.getRequire() != null) {
                    val filterValue = vendorFormContainer.findViewWithTag<FormSelectField>(dynamicSelect?.getRequire()!!.require).getValue()
                    dynamicSelect?.dynamicFilter(filterValue)
                }
                Handler(Looper.getMainLooper()).postDelayed({
                    formInput?.notifyDataSetChange()
                }, 500)
            }
        })

        dynamicSelect?.setOnSelectedListener(object : FormSelectField.FormSelectFieldListener {
            override fun onOptionSelected(option: Option, require: String?) {
               // API data
            }
        })

        parentSelect?.setOnSelectedListener(object : FormSelectField.FormSelectFieldListener {
            override fun onOptionSelected(option: Option, require: String?) {
                if (require != null) {
                    val filterValue = vendorFormContainer.findViewWithTag<FormSelectField>(require).getValue()
                    parentSelect?.filter(filterValue)
                }
                childSelect?.notifyDataSetChange()
                Handler(Looper.getMainLooper()).postDelayed({
                    formInput?.notifyDataSetChange()
                }, 500)
            }
        })

        childSelect?.setOnSelectedListener(object : FormSelectField.FormSelectFieldListener {
            override fun onOptionSelected(option: Option, require: String?) {
                if (require != null) {
                    val filterValue = vendorFormContainer.findViewWithTag<FormSelectField>(require).getValue()
                    childSelect?.filter(filterValue)
                }
                subChildSelect?.notifyDataSetChange()
                Handler(Looper.getMainLooper()).postDelayed({
                    formInput?.notifyDataSetChange()
                }, 500)
            }
        })

        subChildSelect?.setOnSelectedListener(object : FormSelectField.FormSelectFieldListener {
            override fun onOptionSelected(option: Option, require: String?) {
                if (require != null) {
                    val filterValue = vendorFormContainer.findViewWithTag<FormSelectField>(require).getValue()
                    subChildSelect?.filter(filterValue)
                }
                Handler(Looper.getMainLooper()).postDelayed({
                    formInput?.notifyDataSetChange()
                }, 500)
            }
        })

        formInput?.setOnChangedListener(object : FormInputField.FormInputFieldListener {
            override fun onOptionChanged(require: String) {
                val view = vendorFormContainer.findViewWithTag<FormSelectField>(require)
                if (view != null) {
                    formInput!!.setValue(view.getValue())
                }
            }
        })


    }

    private fun showBlockingProgress(show: Boolean) {
        showProgressDialog(show)
    }

    private fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)
    }

    private fun showMessage(message: String) {
        systemMessageNotifier.send(message)
    }

    override fun onBackPressed() {
        Navigation
            .findNavController(requireActivity(), R.id.nav_host_fragment)
            .popBackStack()
    }

    companion object {
        const val VENDOR_ID = "vendor_id"
        const val VENDOR_NAME = "vendor_name"
        const val VENDOR_LOGO = "vendor_logo"

    }
}