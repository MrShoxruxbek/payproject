package uz.ssd.sdk.ui.global.form

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_form_select.view.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.Option
import uz.ssd.sdk.entity.vendor.VendorForm
import uz.ssd.sdk.extension.showSnackMessage
import uz.ssd.sdk.extension.visible
import uz.ssd.sdk.ui.global.spinner.VendorFromSpinnerAdapter

internal class FormSelectField(
    mContext: Context,
    private val vendorForm: VendorForm,
    private val listener: FormChangeListener
) : FrameLayout(mContext), FormItem {

    private var adapter: VendorFromSpinnerAdapter
    private var selectedOption: Option? = null
    private var selectListener: FormSelectFieldListener? = null

    init {
        inflate(context, R.layout.layout_form_select, this)
        tvLabel.text = vendorForm.label
        tvLabel.visible(vendorForm.label != null)
        adapter = VendorFromSpinnerAdapter(context)
        vendorForm.options?.let {
            adapter.setItems(it)
        }

        spinner.adapter = adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedOption = adapter.getItem(position)
                selectedOption?.let {
                    selectListener?.onOptionSelected(it, vendorForm.require)
                }
                listener.onFormChanged()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    fun setOnSelectedListener(listener: FormSelectFieldListener) {
        this.selectListener = listener
    }

    fun notifyDataSetChange() {
        selectedOption?.let {
            selectListener?.onOptionSelected(it, vendorForm.require)
        }
    }

    override fun getKey(): String = vendorForm.key

    override fun getValue(): String = selectedOption?.value ?: ""

    override fun setValue(value: String) {
        vendorForm.options?.let {
            for (option in it) {
                if (option.value == value) {
                    selectedOption = option
                    selectListener?.onOptionSelected(option, vendorForm.require)
                    listener.onFormChanged()
                    break
                }
            }
        }
    }

    fun filter(require: String) {
        vendorForm.options?.let {
            val options = vendorForm.options.filter { it.filter == require } ?: emptyList()
            adapter.setItems(options)
        }
    }

    fun getRequire() = vendorForm.visible

    fun dynamicFilter(require: String) {
        if (vendorForm.visible != null) {
            //get Data from API
            if (require.matches(vendorForm.visible.value!!.toRegex())) {
                this.visible(true)
                showSnackMessage(require)
                adapter.setItems(arrayOf(Option(require, require, null,null,null,null)).toList())
            } else {
                this.visible(false)
            }
        }
    }

    override fun getLabel(): String = vendorForm.label ?: ""

    override fun getValueForInfo(): String = selectedOption?.title ?: ""

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        spinner.isEnabled = enabled
    }

    override fun isValid(): Boolean = selectedOption != null

    interface FormSelectFieldListener {
        fun onOptionSelected(option: Option, require: String?)
    }
}