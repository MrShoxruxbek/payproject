package uz.ssd.sdk.ui.vendor

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_vendors_list.*
import kotlinx.android.synthetic.main.layout_base_list.*
import kotlinx.android.synthetic.main.layout_zero.*
import kotlinx.coroutines.flow.collect
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.server.ResultWrapper
import uz.ssd.sdk.entity.vendor.Vendor
import uz.ssd.sdk.extension.fadeIn
import uz.ssd.sdk.extension.obtainViewModel
import uz.ssd.sdk.extension.visible
import uz.ssd.sdk.ui.global.BaseFragment
import uz.ssd.sdk.ui.global.ZeroViewHolder
import uz.ssd.sdk.ui.global.decoration.linear.StartOffsetItemDecoration
import uz.ssd.sdk.ui.vendor.adapter.CategoryVendorsListAdapter
import uz.ssd.sdk.ui.vendor.form.VendorFormFragment
import uz.ssd.sdk.view_model.vendor.VendorListViewModel

internal class VendorsListFragment : BaseFragment() {
    override val layoutRes = R.layout.fragment_vendors_list

    private lateinit var viewModel: VendorListViewModel
    private var zeroViewHolder: ZeroViewHolder? = null

    private val adapter: CategoryVendorsListAdapter by lazy {
        CategoryVendorsListAdapter {
            openForm(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = obtainViewModel(VendorListViewModel::class.java, scope)
        viewModel.refreshCategoryVendors()
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
            adapter = this@VendorsListFragment.adapter
            visible(false)
        }
        recyclerView.addItemDecoration(StartOffsetItemDecoration(resources.getDimensionPixelOffset(R.dimen.category_vendor_list_start_offset)))

        swipeToRefresh.setOnRefreshListener { viewModel.refreshCategoryVendors() }
        zeroViewHolder = ZeroViewHolder(zeroLayout) { viewModel.refreshCategoryVendors() }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chatUiState.collect {
                when (it) {
                    is ResultWrapper.GenericError -> {
                        showEmptyProgress(false)
                        showEmptyView(false)
                        showEmptyError(true, it.error)
                    }
                    is ResultWrapper.Success -> {
                        showEmptyProgress(false)
                        showEmptyView(false)
                        showEmptyError(false, null)
                        showCategoryVendors(it.value)
                    }
                    is ResultWrapper.Loading -> {
                        showEmptyView(false)
                        showEmptyError(false, null)
                        showEmptyProgress(it.showProgress)
                    }
                    else -> {
                        showEmptyView(true)
                    }
                }
            }
        }
    }

    private fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        postViewAction { swipeToRefresh.isRefreshing = false }
    }

    private fun showEmptyView(show: Boolean) {
        if (show) zeroViewHolder?.showEmptyData()
        else zeroViewHolder?.hide()
    }

    private fun showEmptyError(show: Boolean, message: String?) {
        if (show) {
            zeroViewHolder?.showEmptyError(message)
            recyclerView.visible(false)
        } else {
            zeroViewHolder?.hide()
        }
    }

    private fun showCategoryVendors(categoriesWithVendors: List<Any>) {
        postViewAction { adapter.setData(categoriesWithVendors) }
        recyclerView.fadeIn()
    }

    private fun openForm(vendor: Vendor) {
        val bundleOf = bundleOf(
            VendorFormFragment.VENDOR_ID to vendor.id,
            VendorFormFragment.VENDOR_NAME to vendor.name,
            VendorFormFragment.VENDOR_LOGO to vendor.logo
        )
        Navigation
            .findNavController(requireActivity(), R.id.nav_host_fragment)
            .navigate(R.id.form, bundleOf)
    }
}