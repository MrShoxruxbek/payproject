package uz.ssd.sdk.ui.global.form

import android.text.Editable
import uz.ssd.sdk.ui.global.TextWatcherWrapper

internal class FormInputFormat(
    private val regex: String,
    private val mask: String,
    private val size: Int,
    private val type: String,
    private val validListener: (Boolean, String) -> Unit
) : TextWatcherWrapper() {
    private var lock = false
    private var isDeleting = false

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        isDeleting = count > after
    }

    override fun afterTextChanged(s: Editable) {
        if (lock) return
        lock = true

        if (type == "price") {
            val text = s.toString().replace("\\D".toRegex(), "")

            s.replace(0, s.length, text)
            var i = s.length - 3
            while (i > 0 && i < s.length) {
                if (s.toString()[i] != ' ') {
                    s.insert(i, " ")
                }
                i -= 3
            }

            lock = false
            validListener
                .invoke(
                    text.matches("^[1-9][0-9]*$".toRegex()),
                    if (text.isNotEmpty()) text else ""
                )
        } else {
            if (!isDeleting && mask.isNotEmpty()) {
                val oldLength = s.length
                if (oldLength < mask.length) {
                    if (mask[oldLength] != '#') {
                        s.append(mask[oldLength])
                    } else if (mask[oldLength - 1] != '#') {
                        s.insert(oldLength - 1, mask, oldLength - 1, oldLength)
                    }
                }
            }

            var text = getTextByMask(s.toString())

            if (type == "float") {
                val indexOnDot = text.indexOf('.')
                if (indexOnDot != -1 && text.length > indexOnDot + 3) {
                    text = text.substring(0, indexOnDot + 3)
                    s.replace(0, s.length, text)
                }
            }

            lock = false
            if (regex.isNotEmpty()) {
                validListener.invoke(text.matches(regex.toRegex()), text)
            } else if (size != 0) {
                validListener.invoke(size == text.length, text)
            } else {
                validListener.invoke(true, text)
            }
        }
    }

    private fun getTextByMask(value: String): String {
        if (mask.isEmpty()) return value
        val res = StringBuilder()
        for (i in mask.indices) {
            val temp = mask.substring(i, i + 1)
            if (temp == "#") {
                if (value.length > i) {
                    res.append(value.substring(i, i + 1))
                }
            }
        }
        return res.toString()
    }
}