package uz.ssd.sdk.ui.global.form

import android.content.Context
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_form_checkbox.view.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.VendorForm

internal class FormCheckboxField(
    mContext: Context,
    private val vendorForm: VendorForm
) : FrameLayout(mContext), FormItem {

    init {
        inflate(context, R.layout.layout_form_checkbox, this)
        checkbox.text = vendorForm.label
    }

    override fun getKey(): String = vendorForm.key

    override fun getValue(): String = if (checkbox.isChecked) "1" else "0"

    override fun setValue(value: String) {
        checkbox.isChecked = value == "1"
    }

    override fun getLabel(): String = vendorForm.label ?: ""

    override fun getValueForInfo(): String =
        if (checkbox.isChecked) context.getString(R.string.yes)
        else context.getString(R.string.no)

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        checkbox.isEnabled = enabled
    }

    override fun isValid(): Boolean = true
}