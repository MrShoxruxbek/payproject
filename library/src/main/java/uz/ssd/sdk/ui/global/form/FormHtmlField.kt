package uz.ssd.sdk.ui.global.form

import android.content.Context
import android.os.Build
import android.text.Html
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_form_html.view.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.VendorForm

internal class FormHtmlField(
    mContext: Context,
    private val vendorForm: VendorForm
) : FrameLayout(mContext), FormItem {

    init {
        inflate(context, R.layout.layout_form_html, this)
        vendorForm.text?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvHtml.text = Html.fromHtml(it, Html.FROM_HTML_MODE_COMPACT)
            } else {
                tvHtml.text = Html.fromHtml(it)
            }
        }
    }

    override fun getKey(): String = vendorForm.key

    override fun getValue(): String = "no_value"

    override fun setValue(value: String) {}

    override fun getLabel(): String = ""

    override fun getValueForInfo(): String = ""

    override fun isValid(): Boolean = true
}