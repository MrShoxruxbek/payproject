package uz.ssd.sdk.ui.global.spinner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.Option

internal class VendorFromSpinnerAdapter(
    context: Context
) : ArrayAdapter<Option>(context, R.layout.item_form_vendor_spinner, mutableListOf()) {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createDropDownView(position, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, parent)
    }

    private fun createDropDownView(position: Int, parent: ViewGroup): View {
        val view = inflater.inflate(R.layout.item_form_vendor_drop_down, parent, false)
        val title = view.findViewById<TextView>(R.id.tvTitle)
        title.text = getItem(position)?.title
        return view
    }

    private fun createItemView(position: Int, parent: ViewGroup): View {
        val view = inflater.inflate(R.layout.item_form_vendor_spinner, parent, false)
        val title = view.findViewById<TextView>(R.id.tvTitle)
        title.text = getItem(position)?.title
        return view
    }

    fun setItems(list: List<Option>) {
        clear()
        addAll(list)
    }
}
