package uz.ssd.sdk.ui.global.form

internal interface FormItem {

    fun getKey(): String

    fun getValue(): String

    fun setValue(value: String)

    fun getLabel(): String

    fun getValueForInfo(): String

    fun isValid(): Boolean

}
