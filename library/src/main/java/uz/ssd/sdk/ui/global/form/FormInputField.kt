package uz.ssd.sdk.ui.global.form

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.text.method.DigitsKeyListener
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_form_input.view.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.Option
import uz.ssd.sdk.entity.vendor.VendorForm
import uz.ssd.sdk.extension.formattedMoney
import uz.ssd.sdk.extension.setTint
import uz.ssd.sdk.extension.visible

internal class FormInputField(
    mContext: Context,
    private val vendorForm: VendorForm,
    private val listener: FormChangeListener
) : FrameLayout(mContext), FormItem {

    private var fieldIsValid = false
    private var validValue: String? = null
    private var inputListener: FormInputFieldListener? = null

    init {
        inflate(context, R.layout.layout_form_input, this)
        tvLabel.text = vendorForm.label
        val keysBuilder = StringBuilder()
        when {
            vendorForm.type == "float" ->
                //            keysBuilder.append(".1234567890")
                etInput.inputType =
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            vendorForm.type == "number" ->
                //            keysBuilder.append("1234567890")
                etInput.inputType = InputType.TYPE_CLASS_NUMBER
            vendorForm.type == "price" -> {
                etInput.keyListener = DigitsKeyListener.getInstance("1234567890 ")
            }
            vendorForm.type == "phone" -> {
                keysBuilder.append("1234567890")
                etInput.inputType = InputType.TYPE_CLASS_NUMBER
            }
            else -> etInput.inputType = InputType.TYPE_CLASS_TEXT
        }
        if (vendorForm.mask != null) {
            for (c in vendorForm.mask) {
                if (c != '#' && keysBuilder.indexOf(c) == -1) {
                    keysBuilder.append(c)
                }
            }
        }
        val keys = keysBuilder.toString()
        if (!TextUtils.isEmpty(keys)) {
            etInput.keyListener = DigitsKeyListener.getInstance(keys)
        }
        if (vendorForm.size != null && vendorForm.size > 0) {
            var mSize = vendorForm.size
            if (vendorForm.mask != null && vendorForm.mask.isNotEmpty()) mSize =
                vendorForm.mask.length
            val filterArray = arrayOfNulls<InputFilter>(1)
            filterArray[0] = InputFilter.LengthFilter(mSize)
            etInput.filters = filterArray
        }
        if (vendorForm.prefix != null && vendorForm.prefix.type == "static") {
            etPrefix.visible(true)
            etPrefix.setText(vendorForm.prefix.value)
        } else if (vendorForm.prefix == null && vendorForm.prefix?.type?:"" == "require" && vendorForm.prefix!!.value != null) {
            inputListener?.onOptionChanged(vendorForm.prefix!!.value!!)
        }
        etInput.addTextChangedListener(FormInputFormat(
            vendorForm.regex ?: "",
            vendorForm.mask ?: "",
            vendorForm.size ?: 0,
            vendorForm.type ?: ""
        ) { valid, value ->
            fieldIsValid = valid
            if (valid) {
                validValue = value
            }
            checkAmountIsValid(value)
            val currency = getCurrency()
            if (currency.isNotEmpty()) {
                if (value.isNotEmpty()) {
                    etInput.suffix = currency
                } else {
                    etInput.suffix = null
                }
            }
            etInput.setTint(
                if (fieldIsValid) {
                    if (vendorForm.error != null) etInput.setError(vendorForm.error, null)
                    R.color.green
                } else {
                    vendorForm.error
                    R.color.red
                }
            )
            listener.onFormChanged()
        })
    }

    private fun showError(show: Boolean, message: String?) {
        tvErrorMsg.visible(show)
        tvErrorMsg.text = message
    }

    private fun checkAmountIsValid(value: String) {
        if (vendorForm.amountType != null && vendorForm.minAmount != null && vendorForm.maxAmount != null) {
            var amount = 0.toDouble()
            try {
                if (value.isNotEmpty()) amount = value.toDouble()
            } catch (e: NumberFormatException) {
            }
            if (amount < vendorForm.minAmount || amount > vendorForm.maxAmount.toDouble()) {
                fieldIsValid = false
                showError(
                    true,
                    context?.getString(
                        R.string.amount_interval,
                        vendorForm.minAmount.formattedMoney(false),
                        vendorForm.maxAmount.formattedMoney(false, tiyinToSum = false)
                    ) + getCurrency()
                )
            } else {
                showError(false, null)
            }
        }
    }


    fun setOnChangedListener(listener: FormInputFieldListener) {
        this.inputListener = listener
    }

    private fun getCurrency(): String = when (vendorForm.amountType) {
        "uzs" -> context.getString(R.string.uzs)
        "usd" -> context.getString(R.string.usd)
        else -> ""
    }

    fun isAmountInput(): Boolean = vendorForm.amountType != null

    fun notifyDataSetChange() {
        if (vendorForm.prefix?.value != null)
            inputListener?.onOptionChanged(vendorForm.prefix.value)
    }

    override fun getKey(): String = vendorForm.key

    override fun getValue(): String = validValue ?: ""

    override fun setValue(value: String) {
        etInput.setText(value)
    }

    override fun getLabel(): String = vendorForm.label ?: ""

    override fun getValueForInfo(): String {
        val prefix = if (vendorForm.prefix?.type == "static") vendorForm.prefix.value else ""
        val text = if (vendorForm.amountType != null) {
            "-" + etInput.text.toString().formattedMoney(true) + getCurrency()
        } else {
            etInput.text.toString()
        }
        return prefix + text
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        etInput.isEnabled = enabled
    }

    override fun isValid(): Boolean = fieldIsValid

    interface FormInputFieldListener {
        fun onOptionChanged(require: String)
    }
}