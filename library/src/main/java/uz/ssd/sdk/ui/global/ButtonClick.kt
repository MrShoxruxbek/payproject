package uz.ssd.sdk.ui.global

import android.os.SystemClock
import android.view.View

abstract class ButtonClick : View.OnClickListener {

    private var mLastClickTime: Long = 0L

    abstract fun onSingleClick(v: View?)

    override fun onClick(v: View?) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) return
        mLastClickTime = SystemClock.elapsedRealtime()
        onSingleClick(v)
    }
}