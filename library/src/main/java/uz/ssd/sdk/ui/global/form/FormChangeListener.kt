package uz.ssd.sdk.ui.global.form

internal interface FormChangeListener {

    fun onFormChanged()
}