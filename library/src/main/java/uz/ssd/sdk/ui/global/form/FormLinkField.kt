package uz.ssd.sdk.ui.global.form

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_form_link.view.*
import uz.ssd.sdk.R
import uz.ssd.sdk.entity.vendor.VendorForm

internal class FormLinkField(
    mContext: Context,
    private val vendorForm: VendorForm
) : FrameLayout(mContext), FormItem {

    init {
        inflate(context, R.layout.layout_form_link, this)
        if (vendorForm.text != null && vendorForm.link != null) {
            val text = vendorForm.text.replace("#link#", "<a href=${vendorForm.link}>${vendorForm.link}</a>")
            val policy = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(text)
            }
            tvLink.text = policy
            tvLink.setOnClickListener {
                mContext.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(vendorForm.link)))
            }
        }
    }

    override fun getKey(): String = vendorForm.key

    override fun getValue(): String = "no_value"

    override fun setValue(value: String) {}

    override fun getLabel(): String = ""

    override fun getValueForInfo(): String = ""

    override fun isValid(): Boolean = true
}