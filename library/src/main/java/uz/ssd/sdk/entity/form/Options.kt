package uz.ssd.sdk.entity.form

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Options(
    val prompts: Prompts?
)