package uz.ssd.sdk.entity.form

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Info(
    val text: String? // Купленные сертификаты действуют только на Global аккаунты, и не будут действовать на Korean
)