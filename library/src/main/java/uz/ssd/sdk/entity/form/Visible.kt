package uz.ssd.sdk.entity.form

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Visible(
    val require: String?, // payType
    val value: String? // ^4$
)