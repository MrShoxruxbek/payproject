package uz.ssd.sdk.entity.vendor

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import uz.ssd.sdk.extension.toInt


@JsonClass(generateAdapter = true)
internal class VendorFormAccount(
    val account: Map<String, String>,
    val amount: Long,
    val merchant_id: String
)