package uz.ssd.sdk.entity.server

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class BaseRequest<DATA>(
    val method: String,
    val params: DATA
)

@JsonClass(generateAdapter = true)
internal class BaseMethod(
    val method: String
)
