package uz.ssd.sdk.entity.vendor

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class Vendor(
    val id: String,
    val name: String,
    @Json(name = "short_name") val shortName: String?,
    val logo: String
)