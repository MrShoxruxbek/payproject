package uz.ssd.sdk.entity.form

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Prefix(
    val output: Boolean?, // false
    val require: String?, // city
    val type: String?, // static
    val value: String?, // +998
    val values: List<Value>?
)