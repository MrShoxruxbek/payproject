package uz.ssd.sdk.entity.server

sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    data class GenericError(val code: String? = null, val error: String? = null) : ResultWrapper<Nothing>()
    data class Loading(val showProgress: Boolean) : ResultWrapper<Nothing>()
    object Empty : ResultWrapper<Nothing>()
}