package uz.ssd.sdk.entity.form
import com.squareup.moshi.Json

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Prompts(
    @Json(name = "amount_disabled")
    val amountDisabled: Boolean?, // true
    @Json(name = "requisite_disabled")
    val requisiteDisabled: Boolean? // true
)