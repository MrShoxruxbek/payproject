package uz.ssd.sdk.entity.vendor

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class CategoryVendor(
    val id: Int,
    val title: String,
    val vendors: List<Vendor> = emptyList()
)