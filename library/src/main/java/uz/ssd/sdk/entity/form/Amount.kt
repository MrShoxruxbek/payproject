package uz.ssd.sdk.entity.form
import com.squareup.moshi.Json

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Amount(
        val calc: String?, // ok
        @Json(name = "_fixed")
        val fixed1: Int?, // 100
        val fixed: Int?, // 2450000
        val max: Long?, // 100000000
        val min: Int?, // 50000
        val require: String?, // denomination_id
        val round: Boolean? // false
)