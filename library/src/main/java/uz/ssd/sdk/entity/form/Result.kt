package uz.ssd.sdk.entity.form

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Result(
        val merchants: List<Merchants>?,
        val types: List<Types>?
)

@JsonClass(generateAdapter = true)
data class Types(
        @Json(name = "cache_size")
        val cacheSize: Int?, // 26
        @Json(name = "cache_version")
        val cacheVersion: Int?, // 33
        val icon: String?, // mobile_operators
        val logo: String?, // https://cdn.payme.uz/merchant-types/android/xhdpi/ic_mobile.png?v=1
        val title: String?, // Мобильные операторы
        val type: Int? // 50
)

@JsonClass(generateAdapter = true)
data class Merchants(
        val active: Boolean?, // true
        @Json(name = "additional_info")
        val additionalInfo: Boolean?, // false
        val date: Long?, // 1399114284039
        @Json(name = "_id")
        val id: String, // 55478199d2c4830936e6c832
        val info: Info?,
        val keywords: List<String>?,
        val logo: String?, // https://cdn.payme.uz/merchants/1da87dec20408b8dec00377f72f53948d43850f0.png
        val myhome: Boolean?, // true
        val name: String, // UzMobile
        val options: Options?,
        val organization: String?, // АК «Узбектелеком»
        @Json(name = "paycom_id")
        val paycomId: String?, // 608bcd608ad987025c606085
        val terminal: Terminal?,
        val type: Int?, // 50
        @Json(name = "uid")
        val ussdId: String?, // 06
        val weight: Double? // 0.20149711999190031
)


@JsonClass(generateAdapter = true)
data class GenerateCheque(
        val cheque: Cheq
)

@JsonClass(generateAdapter = true)
data class Cheq(
        @Json(name = "_id")
        val id: String
)
