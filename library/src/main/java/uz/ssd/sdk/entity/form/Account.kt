package uz.ssd.sdk.entity.form

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import uz.ssd.sdk.entity.vendor.Option

@JsonClass(generateAdapter = true)
data class Account(
    val content: String?, // phone
    val default: Any?, // null
    val error: Any?, // null
    val length: Int?, // 9
    val main: Boolean?, // true
    val name: String, // phone
    val optional: Boolean?, // false
    val placeholder: String?,
    val prefix: Prefix?,
    val replace: String?, // [ -]
    val require: String?, // null
    val requires: List<String>?,
    val scan: Any?, // null
    val title: String?, // Номер телефона
    val type: String?, // text
    val validate: Any?, // null
    val validation: String?, // ^(\+?998)?((99)[0-9]{7}|(95)0[0-9]{6}|(95)[^0][0-9]{6})$
    @Json(name = "validation_error")
    val validationError: String?, // Ошибка в значении инвойса
    val value: String?, // +998
    val values: List<Option>?,
    val visible: Visible?, // null
    @Json(name = "visible_")
    val visible1: Visible?
)