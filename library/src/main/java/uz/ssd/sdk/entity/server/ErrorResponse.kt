package uz.ssd.sdk.entity.server

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class ErrorResponse(
    val code: Int,
    val message: String
)