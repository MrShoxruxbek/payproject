package uz.ssd.sdk.entity.server

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class BaseResponse<DATA>(
    val jsonrpc: String="2.0",
    val result: DATA? = null,
    val error: ErrorResponse? = null
) {
    fun isSuccessful(): Boolean = error == null && result != null
    fun isFailure(): Boolean = error != null && result == null
}