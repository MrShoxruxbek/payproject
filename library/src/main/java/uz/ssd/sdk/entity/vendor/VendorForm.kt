package uz.ssd.sdk.entity.vendor

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import uz.ssd.sdk.entity.form.Prefix
import uz.ssd.sdk.entity.form.Visible

internal class VendorForm(
    val key: String,
    val show: Int = 1,
    val label: String?,
    val element: String?,
    val type: String?,
    val value: String?,
    val mask: String?=null,
    val prefix: Prefix?=null,
    val regex: String?=null,
    val placeholder: String?=null,
    val text: String? = "Link",
    val link: String?=null,
    val visible: Visible?=null,
    val size: Int?=null,
    val error: String?=null,
    val require: String?=null,
    val requires: List<String>?=null,
    @Json(name = "amount_type") val amountType: String?=null,
    @Json(name = "min_amount") val minAmount: Int?=null,
    @Json(name = "max_amount") val maxAmount: Long?=null,
    val options: List<Option>?=null
)

@JsonClass(generateAdapter = true)
class Option(
    val title: String,
    val value: String,
    val id: String? = value,
    val filter: Any?,
    val amount: Int?,
    val name: String?
)