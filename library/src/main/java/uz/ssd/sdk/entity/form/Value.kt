package uz.ssd.sdk.entity.form

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Value(
    val filter: String?, // 1
    val title: String?, // HE
    val value: String? // he-
)