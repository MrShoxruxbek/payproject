package uz.ssd.sdk.entity.form
import com.squareup.moshi.Json

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Terminal(
    val account: List<Account>?,
    val amount: Amount?,
    val endpoint: String?, // https://book.uzairways.com/ru/?op=%D0%9A%D1%83%D0%BF%D0%B8%D1%82%D1%8C+%D0%B1%D0%B8%D0%BB%D0%B5%D1%82
    @Json(name = "link_type")
    val linkType: String?, // external
    val type: String? // form
)