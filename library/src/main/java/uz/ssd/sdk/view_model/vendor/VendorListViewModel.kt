package uz.ssd.sdk.view_model.vendor

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import uz.ssd.sdk.entity.server.ResultWrapper
import uz.ssd.sdk.entity.vendor.CategoryVendor
import uz.ssd.sdk.model.interactor.VendorInteractor
import uz.ssd.sdk.view_model.global.ErrorHandler
import uz.ssd.sdk.view_model.global.BaseViewModel
import javax.inject.Inject

internal class VendorListViewModel @Inject constructor(
    private val vendorInteractor: VendorInteractor,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    private val _chatUiState = MutableStateFlow<ResultWrapper<List<CategoryVendor>>>(ResultWrapper.Empty)
    val chatUiState: StateFlow<ResultWrapper<List<CategoryVendor>>> = _chatUiState

    fun refreshCategoryVendors() {
        vendorInteractor.getAllCategory()
            .doOnSubscribe {
                _chatUiState.value = ResultWrapper.Loading(true)
            }.doAfterTerminate {
                _chatUiState.value = ResultWrapper.Loading(false)
            }.subscribe({ categories ->
                if (categories.isNotEmpty()) {
                    _chatUiState.value = ResultWrapper.Success(categories)
                } else {
                    _chatUiState.value = ResultWrapper.Empty
                }
            }, { error ->
                errorHandler.proceed(error) {
                    _chatUiState.value = ResultWrapper.GenericError(code = "200", error = it)
                }
            }).connect()
    }
}