package uz.ssd.sdk.view_model.global

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }

    protected fun Disposable.connect() {
        disposable.add(this)
    }

}