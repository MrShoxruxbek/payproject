package uz.ssd.sdk.view_model.vendor.form

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import uz.ssd.sdk.entity.server.ResultWrapper
import uz.ssd.sdk.entity.vendor.VendorForm
import uz.ssd.sdk.entity.vendor.VendorFormAccount
import uz.ssd.sdk.model.interactor.VendorInteractor
import uz.ssd.sdk.view_model.global.ErrorHandler
import uz.ssd.sdk.view_model.global.BaseViewModel
import javax.inject.Inject

internal class VendorFormViewModel @Inject constructor(
    private val vendorInteractor: VendorInteractor,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    private var vendorFormList: List<VendorForm>? = null

    private val _formUiState = MutableStateFlow<ResultWrapper<List<VendorForm>>>(ResultWrapper.Empty)
    val formUiState: StateFlow<ResultWrapper<List<VendorForm>>> = _formUiState

    private val _payStatusUiState = MutableStateFlow<ResultWrapper<String>>(ResultWrapper.Empty)
    val payStatusUiState: StateFlow<ResultWrapper<String>> = _payStatusUiState

    fun refreshVendorForm(vendorId: String) {
        vendorInteractor.getVendorForm(vendorId)
            .doOnSubscribe {
                _formUiState.value = ResultWrapper.Loading(true)
            }.doAfterTerminate {
                _formUiState.value = ResultWrapper.Loading(false)
            }.subscribe({ from ->
                if (from.isNotEmpty()) {
                    vendorFormList = from
                    _formUiState.value = ResultWrapper.Success(from)
                } else {
                    _formUiState.value = ResultWrapper.Empty
                }
            }, { error ->
                errorHandler.proceed(error) {
                    _formUiState.value = ResultWrapper.GenericError(code = "200", error = it)
                }
            })
            .connect()
    }

    fun vendorFormCheck(
        vendorFormParams: HashMap<String, String>, amount: Long, vendorId: String
    ) {
        val req = VendorFormAccount(vendorFormParams, amount, vendorId)
        vendorInteractor.vendorFormCheck(req)
            .doOnSubscribe {
                _payStatusUiState.value = ResultWrapper.Loading(true)
            }.doAfterTerminate {
                _payStatusUiState.value = ResultWrapper.Loading(false)
            }.subscribe({
                _payStatusUiState.value = ResultWrapper.Success(it)
            }, { error ->
                errorHandler.proceed(error) {
                    _payStatusUiState.value = ResultWrapper.GenericError(code = "200", error = it)
                }
            })
            .connect()
    }
}