package uz.ssd.sdk.view_model.global

import timber.log.Timber
import uz.ssd.sdk.extension.userMessage
import uz.ssd.sdk.model.data.server.ServerError
import uz.ssd.sdk.model.system.ResourceManager
import javax.inject.Inject

internal class ErrorHandler @Inject constructor(
    private val resourceManager: ResourceManager
) {

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        Timber.e(error)
        if (error is ServerError) {
            messageListener(error.error.message)
        } else {
            messageListener(error.userMessage(resourceManager))
        }
    }

}
