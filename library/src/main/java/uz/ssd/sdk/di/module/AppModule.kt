package uz.ssd.sdk.di.module

import android.content.Context
import android.content.res.AssetManager
import toothpick.config.Module
import uz.ssd.sdk.di.CacheLifetime
import uz.ssd.sdk.di.DefaultPageSize
import uz.ssd.sdk.di.PrimitiveWrapper
import uz.ssd.sdk.di.provider.ResourceManagerProvider
import uz.ssd.sdk.model.system.AppSchedulers
import uz.ssd.sdk.model.system.ResourceManager
import uz.ssd.sdk.model.system.SchedulersProvider
import uz.ssd.sdk.model.system.message.SystemMessageNotifier

internal class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(PrimitiveWrapper::class.java).withName(DefaultPageSize::class.java).toInstance(PrimitiveWrapper(20))
        bind(PrimitiveWrapper::class.java).withName(CacheLifetime::class.java).toInstance(PrimitiveWrapper(300_000L))
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).toProvider(ResourceManagerProvider::class.java)
        bind(AssetManager::class.java).toInstance(context.assets)
        bind(SystemMessageNotifier::class.java).toInstance(SystemMessageNotifier())
    }
}