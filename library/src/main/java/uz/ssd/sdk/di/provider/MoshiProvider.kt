package uz.ssd.sdk.di.provider

import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

internal class MoshiProvider @Inject constructor() : Provider<Moshi> {

    override fun get(): Moshi =
        Moshi.Builder().build()
}