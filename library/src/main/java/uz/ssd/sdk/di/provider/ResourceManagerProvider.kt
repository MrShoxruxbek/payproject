package uz.ssd.sdk.di.provider

import android.content.Context
import uz.ssd.sdk.model.system.LocaleManager
import uz.ssd.sdk.model.system.ResourceManager
import javax.inject.Inject
import javax.inject.Provider

internal class ResourceManagerProvider @Inject constructor(
    private val context: Context
) : Provider<ResourceManager> {

    override fun get() = ResourceManager(LocaleManager.setLocale(context)!!)

}