package uz.ssd.sdk.di.module

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import toothpick.config.Module
import uz.ssd.sdk.di.ServerPath
import uz.ssd.sdk.di.provider.ApiProvider
import uz.ssd.sdk.di.provider.MoshiProvider
import uz.ssd.sdk.di.provider.OkHttpClientProvider
import uz.ssd.sdk.model.data.cache.VendorsListCache
import uz.ssd.sdk.model.data.server.BillingApi
import uz.ssd.sdk.model.interactor.VendorInteractor
import uz.ssd.sdk.view_model.global.ErrorHandler

internal class ServerModule : Module() {
    init {
        //Network
        bind(String::class.java).withName(ServerPath::class.java).toInstance("https://payme.uz")
        bind(Moshi::class.java).toProvider(MoshiProvider::class.java).providesSingleton()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).providesSingleton()
        bind(VendorsListCache::class.java).singleton()
        bind(BillingApi::class.java).toProvider(ApiProvider::class.java).providesSingleton()

        //Error handler with logout logic
        bind(ErrorHandler::class.java)
        bind(VendorInteractor::class.java).singleton()
    }
}