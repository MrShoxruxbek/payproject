package uz.ssd.sdk.di.provider

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import uz.ssd.sdk.di.ServerPath
import uz.ssd.sdk.model.data.cache.VendorsListCache
import uz.ssd.sdk.model.data.server.BillingApi
import uz.ssd.sdk.model.data.server.ApiWithCache
import javax.inject.Inject
import javax.inject.Provider

internal class ApiProvider @Inject constructor(
    private val okHttpClient: OkHttpClient,
    private val moshi: Moshi,
    private val vendorsListCache: VendorsListCache,
    @ServerPath private val serverPath: String
) : Provider<BillingApi> {

    override fun get() =
        ApiWithCache(
            Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .client(okHttpClient)
                .baseUrl(serverPath)
                .build()
                .create(BillingApi::class.java),
            vendorsListCache
        )
}