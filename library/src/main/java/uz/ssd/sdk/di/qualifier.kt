package uz.ssd.sdk.di

import javax.inject.Qualifier

@Qualifier
internal annotation class DefaultPageSize

@Qualifier
internal annotation class ServerPath

@Qualifier
internal annotation class CacheLifetime