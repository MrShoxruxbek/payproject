package uz.ssd.sdk.di.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import timber.log.Timber
import toothpick.Scope
import toothpick.Toothpick
import uz.ssd.sdk.di.DI
import uz.ssd.sdk.ui.global.BaseFragment
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
        val app: Context
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return Toothpick.openScope(DI.APP_SCOPE)
            .getInstance(modelClass) as T
    }

}