-keep class uz.ssd.sdk.model.data.server.ApiWithCache
# Specifies that string constants that correspond to class names should be obfuscated as well.
-adaptclassstrings uz.ssd.sdk.**

# Note that if we could use kapt to generate registries, possible to get rid of this
-keepattributes *Annotation*
# Do not obfuscate classes with Injected Constructors
-keepclasseswithmembernames class * { @javax.inject.Inject <init>(...); }
# Do not obfuscate classes with Injected Fields
-keepclasseswithmembernames class * { @javax.inject.Inject <fields>; }
# Do not obfuscate classes with Injected Methods
-keepclasseswithmembernames class * { @javax.inject.Inject <methods>; }

-keep class **__Factory { *; }
-keep class **__MemberInjector { *; }