package uz.ssd.pay;

import android.app.Application;
import android.content.Context;

import uz.ssd.sdk.SSDBilling;
import uz.ssd.sdk.model.system.LocaleManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SSDBilling.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocaleRu(base));
    }

}
